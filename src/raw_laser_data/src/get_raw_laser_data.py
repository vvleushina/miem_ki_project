#!/usr/bin/env python2
""" Listener for arbitrary messages """

import math
import sys
# library to generate command 'import ___' at runtime
from importlib import import_module
from std_msgs.msg import String
from sensor_msgs.msg import PointCloud, LaserScan
#from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point32
import laser_geometry.laser_geometry as lg

import rospy

import tf

class LaserData(object):
    """ Based on example of subscription to topic of arbitrary type
    from https://schulz-m.github.io/2016/07/18/rospy-subscribe-to-any-msg-type/"""
    def __init__(self):
         self.listener = tf.TransformListener()
         self._init_sub = rospy.Subscriber(
            'laser', Laser, self.laser_callback)
         self._init_pub = rospy.Publisher(
            'laser_scan', PointCloud, queue_size=10)
         
        

    def laser_callback(self, data):
        #if data.header.frame_id == 'ant1_left_rf_link':
         #   topic_from = '/ant1/ant1_left_rf_link'
        #else:
         #   topic_from = '/ant1/ant1_right_rf_link'
        topic_from = data.header.frame_id
            
        #rospy.logwarn('frame:' + data.header.frame_id)
        #rospy.logwarn('value: ' + str(data.range))
        
        (trans, rot) = self.listener.lookupTransform(topic_from, '/map', rospy.Time(0))
        #rospy.logwarn('trans: ' + str(trans))
        pc = PointCloud()
        px = trans[0]
        py = trans[1]
        p1 = Point32(px, py, trans[2])
        
        #lp = lg.LaserProjection()
        #pc2_msg = lp.projectLaser(msg)
        
        rot_angle = 2*math.atan2(rot[2], rot[3])
        rospy.logwarn('{} {}, {}'.format(px, py, rot_angle*180/3.14))
        p2 = Point32(px + data.range*math.cos(rot_angle), py + data.range*math.sin(rot_angle), trans[2])
        pc.points = [p1, p2]
        pc.header.frame_id = 'ant1/odom'
        self._init_pub.publish(pc)
        
        

if __name__ == '__main__':
    rospy.init_node('laser_data')
    al = LaserData()
    r = rospy.Rate(10)
    r.sleep()
    
    rospy.spin()
    
