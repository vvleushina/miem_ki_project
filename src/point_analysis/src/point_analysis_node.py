#!/usr/bin/env python2
""" Classificator for points (if they are belonged to one obstacle) """


import sys
# library to generate command 'import ___' at runtime
from importlib import import_module
from std_msgs.msg import String
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Point
from point_analysis.msg import Wall, WallList

import rospy

import numpy as np
import tensorflow as tf
import keras
from tensorflow.keras import backend as K
from keras.models import Sequential 
from keras.layers import Dense, Activation, Dropout
from keras.layers.advanced_activations import LeakyReLU
from keras import regularizers
import keras.backend.tensorflow_backend as tb
#tb._SYMBOLIC_SCOPE.value = True

    

class PointClassification(object):
    def __init__(self):
        # create listener for topic 'example' with arbitrary type
        self._init_sub = rospy.Subscriber(
            'default_robot/laser_scan', LaserScan, self.callback)
        self._init_pub = rospy.Publisher(
            'preprocessed_data', WallList, queue_size=10)
    """
    def reset_tf_session(self):
        curr_session = tf.get_default_session()
        if curr_session is not None:
            curr_session.close()
        tf.keras.backend.clear_session()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        s = tf.InteractiveSession(config=config)
        tf.keras.backend.set_session(s)
        return s
    """

    def callback(self, msg):
        #s = self.reset_tf_session()
        lines = []
        points = []
        initializer = 'glorot_normal'
        
        model = Sequential()
        model.add(Dense(512, kernel_initializer=initializer, input_shape=(6,)))
        model.add(LeakyReLU(0.1))
        model.add(Dropout(0.4))
        model.add(Dense(256, kernel_initializer=initializer))
        model.add(LeakyReLU(0.1))
        model.add(Dropout(0.2))
        model.add(Dense(32, kernel_initializer=initializer))
        model.add(Dense(2, kernel_initializer=initializer))
        model.add(Activation("softmax"))
        
        model.load_weights('/home/korchagina-av/weights.hdf5', by_name=False)
        
        #rospy.logwarn(msg.ranges)
        
        
        
        n = len(msg.ranges)
        i = 0
        while i < n-5:
            data = []
            for j in range(i, i+6):
                data.append(msg.ranges[j])
                #rospy.logwarn(msg.ranges[j])
                #rospy.logwarn(type(msg.ranges[j]))
            rospy.logwarn(data)
            #rospy.logwarn(type(data))
            y = model.predict_classes(np.array([data]))[0]
            rospy.loginfo('Predicted "' + str(y) + '"')
            
            if y == 0:
                lines.append([data[0], data[-1]])
            
            
            i += 1
        
        out_msg = WallList()
        for l in lines:
          w = Wall()
          w.wall_points = [l[0], l[1]]
          out_msg.walls.append(w) 
          
          
        self._init_pub.publish(out_msg)
        
        """
        data_yaml = yaml.dump(msg, width=1000)
        
        # print results
        rospy.loginfo('Result1 is "' + data_yaml+ '"')
        s = String()
        s.data = data_yaml
        self._init_pub.publish(s)
        """
        

if __name__ == '__main__':
    rospy.init_node('point_analisys')
    pclf = PointClassification()
    rospy.spin()
    
