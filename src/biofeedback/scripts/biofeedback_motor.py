#!/usr/bin/env python
import rospy
import biofeedback_operating_mode_rules

class Motors:
    def __init__(self, _t0, _t1, _N0, _N1):
        self.t0 = _t0;
        self.t1 = _t1;
        self.N0 = _N0;
        self.N1 = _N1;
        
