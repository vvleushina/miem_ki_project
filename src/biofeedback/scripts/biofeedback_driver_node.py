#!/usr/bin/env python
import rospy
from std_msgs.msg import ByteMultiArray

import serial

class FeedbackDevice:
    def __init__(self):
        pass
    def load_parameters(self):
        self.device_name=rospy.get_param('~device_name','/dev/ttyUSB0')
    def run(self):
        # create node
        rospy.init_node('biofeedback_driver')
        self.load_parameters()
        # connect with device
        self.port = serial.Serial(self.device_name,9600)
        # start data processing
        rospy.Subscriber("biofeedback", ByteMultiArray, self.callback)
        # start processing
        rospy.spin()
        self.port.close()
    def callback(self,msg):
        if all([(x==0 or x==1) for x in msg.data]):
            dev_message = ''.join(['#{}-{}'.format(i,x)  for i,x in enumerate(msg.data)])
            self.port.write(dev_message)
    
if __name__=='__main__':
    dev = FeedbackDevice()
    dev.run()


