#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import yaml
import biofeedback_motor

from std_msgs.msg import ByteMultiArray
from sensor_msgs.msg import Range



class FeedbackTranslator:
    def __init__(self):
        pass

    def load_parameters(self):
        self.threshold = rospy.get_param('~threshold', 0.2)
        self.input_topic = rospy.get_param('~input_topic', 'range_sensor')
        self.config_name = rospy.get_param('~config_name')
        self.config_mode_name = rospy.get_param('~config_mode_name')
        self.sensor_mapping = rospy.get_param('sensor_mapping', {'sonar_left':0, 'sonar_right':1})
        # check if mapping is empty
        if self.sensor_mapping=={}:
            raise ValueError('sensor mapping is empty; add pairs of range and feedback motors in sensor_mapping variable')
        out_size = max([self.sensor_mapping[k]
                        for k in self.sensor_mapping.keys()]) + 1
        """
        List of rules for transition from one state to another state:
        ---state - initial state
        ---t - duration level of logical unit
        ---T - amount of signals in pack
        ---mode - certain motor mode
        """
        self.rules = [(lambda state, t, T, mode : (state == 'S0',                'S1', True, 0, 0)),
                      (lambda state, t, T, mode : (state == 'S1' and t+1<mode.t1, 'S1', True, t+1, T)),
                      (lambda state, t, T, mode : (state == 'S1' and t+1>=mode.t1,'S2', False, 0, T)),
                      (lambda state, t, T, mode : (state == 'S2' and t+1<mode.t0,'S2', False, t+1, T)),
                      (lambda state, t, T, mode : (state == 'S2' and t+1>=mode.t0 and T+1<mode.N1,'S1', True, 0, T+1)),
                      (lambda state, t, T, mode : (state == 'S2' and T+1>=mode.N1,'S3', False, 0, 0)),
                      (lambda state, t, T, mode : (state == 'S3' and T+1<mode.N0,'S3', False, 0, T+1)),
                      (lambda state, t, T, mode : (state == 'S3' and T+1>=mode.N0,'S1', True, 0, 0))]
        self.target_mode = [0]*7 #array of working mode for each motor
        self.t = [0]*7 #array of duration the level of logical unit for each motor
        self.T = [0]*7 #array of amount of signals in pack for each motor
        self.state = ["S0"]*7 #array of state motor for each motor
        self.motors_work = [0]*7 #array for determination work each motor now or not
        
    def run(self):
        # create node
        rospy.init_node('biofeedback_control')
        #initializate parameters
        self.load_parameters()
        with open(self.config_name) as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader)
        with open(self.config_mode_name) as file:
            self.config_mode_name = yaml.load(file, Loader=yaml.FullLoader)
        #read motors states from file "operating_mode.yaml"
        self.motors = [biofeedback_motor.Motors(mode['t0'],mode['t1'],mode['T0'], mode['T1']) for mode in self.config_mode_name]
        # create publisher for driver
        self.pub = rospy.Publisher('biofeedback', ByteMultiArray, queue_size=1)
        # start data processing
        rospy.Subscriber(self.input_topic, Range, self.range_callback)
        # start timer
        rospy.Timer(rospy.Duration(0.1), self.timer_callback)
        # process messages
        rospy.spin()

    def range_callback(self,msg):
        #define working mode for each motor
        motor_number = 0
        for motor in self.config:
            motor_number = motor_number+1
            for sensor in self.config.get(motor).get('Sensors'):
                if msg.header.frame_id == sensor:
                    count = 0
                    for value in self.config.get(motor).get('Value'):
                        if msg.range > value:
                            count = count+1
                    self.target_mode[motor_number-1] = self.config.get(motor).get('Operation_mode')[count-1]

    def timer_callback(self, event):
        #function for transition each motor from one state to another state
        msg = ByteMultiArray()
        #cycle through every motor
        for number in range(len(self.target_mode)):
            #if motor has working mode, then start algorithm for transition each motor from one state to another state
            if(self.target_mode[number] > 0):
                count = 0
                #cycle through every rule
                for ind, rule in enumerate(self.rules):
                    (perm, state_to, motors_work, t, T)= rule(self.state[number], self.t[number], self.T[number], self.motors[self.target_mode[number]])
                    #if transition is permitted
                    if perm:
                        #upgrade information (process of transition)
                        self.state[number] = state_to
                        self.motors_work[number] = motors_work
                        if motors_work:
                            count += 1
                        self.t[number] = t
                        self.T[number] = T
                        break
            if(self.target_mode[number] == 0):
                self.motors_work[number] = 0;
        msg.data =  self.motors_work
        self.pub.publish(msg)
    
if __name__=='__main__':
    try:
        tr = FeedbackTranslator()
    except ValueError as e:
        ROS_ERROR(str(e))
        exit(0)
    tr.run()


