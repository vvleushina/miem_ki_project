#!/usr/bin/env python2
""" Listener for arbitrary messages """
import yaml
import json

import StringIO


import sys
# library to generate command 'import ___' at runtime
from importlib import import_module
from std_msgs.msg import String

import rospy

class ArbitraryListener(object):
    """ Based on example of subscription to topic of arbitrary type
    from https://schulz-m.github.io/2016/07/18/rospy-subscribe-to-any-msg-type/"""
    def __init__(self):
        self._init_sub = rospy.Subscriber(
            'input', rospy.AnyMsg, self.setup_callback)
        self._init_pub = rospy.Publisher(
            'to_vr', String, queue_size=10)

    def setup_callback(self, msg):
        #import_module needs at least python 2.7
        assert(sys.version_info >= (2,7))
        # get header from the first message received from the topic
        connection_header =  msg._connection_header['type'].split('/')
        # connection_header consists of package name and message name; get it
        msg_package = connection_header[0] + '.msg'
        self.msg_type = connection_header[1]
        rospy.loginfo('Message type detected as ' + self.msg_type)
        # dynamically generate and execute command
        # 'from msg_package.msg import msg_type'
        # and get class object from the result
        msg_class = getattr(import_module(msg_package), self.msg_type)
        # add package name to message type string 
        self.msg_type = connection_header[0] + '/' + self.msg_type
        # stop old subscription to the input topic that has type AnyMsg
        self._init_sub.unregister()
        # subscribe on the same topic with calculated type
        self._main_sub = rospy.Subscriber(
            'input', msg_class, self.deserialized_callback)
        

    def deserialized_callback(self, msg):
        # print the message received
        # rospy.loginfo(str(msg.data))
        
        #data_yaml = yaml.dumps(msg)
        #data_yaml = json.dumps(msg.__dict__)
        
        #data_yaml = ' '.join([f for f in dir(msg) if not callable(f)])
        out_dict = {'type': self.msg_type,
                    'data': yaml.load(str(msg))}
        data_json = json.dumps(out_dict,indent=4)
        # print results
        rospy.loginfo('Result is "' + data_json + '"')
        s = String()
        s.data = data_json
        self._init_pub.publish(s)
        

if __name__ == '__main__':
    rospy.init_node('json_serialization')
    al = ArbitraryListener()
    rospy.spin()
    
