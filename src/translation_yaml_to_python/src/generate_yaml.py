#!/usr/bin/env python2
import yaml
import rospy

from std_msgs.msg import String

class GenerateYaml:
    def __init__(self):
        self.pub = rospy.Publisher('Sender', String, queue_size=1)
    def run(self):
        #---wait
        r = rospy.Rate(10)
        r.sleep()
        #---stop wait
        self.example = String()
        self.example.data = {
            "Motor_1": {
                "Sensors": ["lf","rf"],
                "Value": [0, 100, 100],
                "Operation_mode": [1]
            }
            "Motor_2":{
                "Sensors": ["lb","rb"],
                "Value": [0, 100, 50],
                "Operation_mode": [1, 2]
            }
        }
        self.yaml_data = yaml.dump(self.example)
        self.pub.publish(self.yaml_data)

if __name__=='__main__':
    rospy.init_node('generate_yaml')
    cmp = GenerateYaml()
    cmp.run()
    rospy.spin()