#!/usr/bin/env python2
import yaml
import rospy

from std_msgs.msg import String

class YamlToPythonTranslator:
    def __init__(self):
        pass
    def run(self):
        #create node
        rospy.init_node('yaml_to_python')
        #create Subscriber
        rospy.Subscriber('Sender', String, self.translator_callback)
        #create Publisher
        self.pub = rospy.Publisher('Reciever', type(self.py_object)[5:(len(type(self.py_object)) - 2)], queue_size=1)
        rospy.spin()

    def translator_callback(self, msg):
        #YAML to python
        self.py_object=yaml.load(msg.data)
        #print result (for testing)
        rospy.loginfo('Result is "' + str(self.py_object) + '"' + '\n')
        self.pub.publish(self.py_object)

if __name__=='__main__':
    cmp = YamlToPythonTranslator()
    cmp.run()
