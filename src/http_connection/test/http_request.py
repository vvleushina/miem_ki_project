#!/usr/bin/env python2
#coding: utf-8
"""
HTTP Server Example
"""

# client library for HTTP protocol
import requests
import time 

# send HTTP message to server on host 127.0.0.1, port 19000 with specified data and timeout;
try: 
    reply = requests.post('http://172.20.203.27:29025', data='Abc', timeout=10.0) 
    print(reply.content)
except:
    time.sleep(5)
