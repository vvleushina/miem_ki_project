#!/usr/bin/env python
import rospy

import unittest

from std_msgs.msg import String

""" The simplest test """
class TransferTest(unittest.TestCase):
    def callback_mirror(self, inp_msg):
        if inp_msg.data != self.data:
            self.flag_error = True
        self.flag_finish = True
    def runTest(self):
        self.data = 'test_message'
        self.flag_finish = False
        self.flag_error = False
        rospy.init_node('test_mirror')
        # create publisher and subscriber
        pub = rospy.Publisher('example_topic', String, queue_size=10)
        rospy.Subscriber('Reciever', String, self.callback_mirror)
        # create and send a test message
        r = rospy.Rate(10)
        r.sleep()
        tm = String()
        tm.data = self.data
        # wait for responce
        while not rospy.is_shutdown() and not self.flag_finish:
            r.sleep()
            pub.publish(tm)
        self.assertFalse(self.flag_error)

class MyTestSuite(unittest.TestSuite):
    def __init__(self):
        super(MyTestSuite, self).__init__()
        self.addTest(TransferTest())
        
