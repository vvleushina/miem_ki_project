#!/usr/bin/env python2
#coding: utf-8
"""
Choose random diagnostic messages from list (config.yaml)
"""
import random
import time 
import rospy
import yaml

from std_msgs.msg import String
from diagnostic_msgs.msg import DiagnosticStatus,KeyValue


#initializate node
rospy.init_node('SentDiagnosticMessages')
pub = rospy.Publisher('exampleDiagnostic', DiagnosticStatus, queue_size=10)

#read file and create diagnostic message
with open('/home/robofob/catkin_ws/src/miem_ki_project/src/http_connection/src/config.yaml') as file:
            config = yaml.load(file, Loader=yaml.SafeLoader)
out_msg = DiagnosticStatus()

while True:
    #add in messag new information
    num = random.randint(0,len(config)-1)
    name = config.keys()[num]
    out_msg.level = config.get(name).get('level')
    out_msg.name = name
    out_msg.message = config.get(name).get('message')
    out_msg.hardware_id = config.get(name).get('hardware_id')
    out_msg.values = []
    for k,v in config.get(name).get('value').items():
        kv = KeyValue()
        kv.key = k
        kv.value = v
        out_msg.values.append(kv)
    
    #publish message and wait
    pub.publish(out_msg)
    time.sleep(5)

