#!/usr/bin/env python2
#coding: utf-8
"""
HTTP server 
"""

import requests
import rospy
# HTTP server library
import BaseHTTPServer as http
from std_msgs.msg import String

import json

data_queue = {}

class RequestHandler(http.BaseHTTPRequestHandler):
    """ an object of this class will be created for each new request """
    def do_POST(self):
        """ handler for POST requests """
        self.process_data()
        
    def do_GET(self):
        """ handler for GET requests """
        self.process_data()
        
    def process_data(self):
        global data_queue
        # read length of data in the request received
        content_len = int(self.headers['Content-Length'])
        if content_len>0:
            rospy.logerr('Input len: ' + str(content_len))
            # read data from request
            content = self.rfile.read(content_len)
            # print the message received
            rospy.loginfo('Received data: '+str(content))
        # reply: send status code "OK"
        self.send_response(200)
        self.end_headers()
        # send responce
        responce = json.dumps(data_queue)
        rospy.logwarn(responce)
        self.wfile.write(responce)
        data_queue = {}
        
def timer_callback(event):
    """ timer """
    global http_server
    # wait for request and initialize processing in RequestHandler
    http_server.handle_request()

def ros_input_callback(msg):
    global data_queue
    # deserialize message
    deserialized_data = json.loads(msg.data)
    # both 'type' and 'data' are required fields
    if 'type' not in deserialized_data.keys():
        rospy.logerr('http_server: input data has no "type" field')
        return
    if 'data' not in deserialized_data.keys():
        rospy.logerr('http_server: input data has no "data" field')
        return
    msg_type = deserialized_data['type']
    rospy.logerr(deserialized_data['type'])
    data_queue[msg_type] = deserialized_data['data']
    
if __name__ == '__main__':
    # initialize ROS node
    rospy.init_node('http_server')
    # create address for the server to wait requests
    try:
        our_ip = rospy.get_param('~ip_address')
    except KeyError:
        # ip address not found; quit
        rospy.logerr('http_server: parameter "ip_address" not found')
        exit(-1)
    try:
        our_port = rospy.get_param('~port')
    except KeyError:
        # ip address not found; quit
        rospy.logerr('http_server: parameter "port" not found')
        exit(-1)
    server_address = (our_ip, our_port)
    # create server for the address specified;
    # requests will be handled by object of class RequestHandler
    http_server = http.HTTPServer(server_address, RequestHandler)
    # start function timer_callback which should start waiting for request to process
    http_timer = rospy.Timer(rospy.Duration(0.1), timer_callback)
    # get data from ROS
    rospy.Subscriber('to_vr', String, ros_input_callback)
    # wait Ctrl-C while processing requests
    rospy.spin()
    # close server
    http_server.server_close()
    
