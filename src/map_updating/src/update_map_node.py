#!/usr/bin/env python2
""" Listener for arbitrary messages """
import yaml

import sys
# library to generate command 'import ___' at runtime
from importlib import import_module
from std_msgs.msg import String
from sensor_msgs.msg import PointCloud
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Point32

import rospy

class MapUpdating(object):
    """ Based on example of subscription to topic of arbitrary type
    from https://schulz-m.github.io/2016/07/18/rospy-subscribe-to-any-msg-type/"""
    def __init__(self):
        # create listener for topic 'example' with arbitrary type
        self._init_sub = rospy.Subscriber(
            'rangefinder', PointCloud, self.deserialized_callback)
        self._init_pub = rospy.Publisher(
            'updated_map', MarkerArray, queue_size=100)
        
        self.old_map = []
        #self.new_map = MarkerArray()
        


    def deserialized_callback(self, data):
        points = []
        for i in range(0, len(data.points) - 1, 2):
            start = (data.points[i].x, data.points[i].y, data.points[i].z)
            end = (data.points[i + 1].x, data.points[i + 1].y, data.points[i + 1].z)
            p = (start, end)
            points.append(p)
            
        # print the message received
        #new_map = MarkerArray()
        new_map = self.update_map(points)
        new_map_markerarray = self.convert(new_map)
        # rospy.loginfo(str(data))
        
        #data_yaml = yaml.dump(new_map)
        
        
        # print results
        #rospy.logerr('Result1 is "' + str(new_map_markerarray)+ '"')
        self._init_pub.publish(new_map_markerarray)
        
    def update_map(self, lines):
        old_map = self.old_map
        new_map = []
        map_set = []
        delete = []
        eps = 1e-3

        # for every new point
        for line in lines:
            x2 = line[1][0] 
            x1 = line[0][0]
            y2 = line[1][1]
            y1 = line[0][1]
            z2 = line[1][2]
            z1 = line[0][2]
            # check if there is an old point kying on the new line
            for old_line in old_map:
                x = old_line[0]
                y = old_line[1]
                z = old_line[2]

                alpha = ((x2-x1) * (x-x1) + (y2 - y1) * (y - y1) + (z2 - z1) * (z - z1)) / ((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2)
                x0 = x1 + alpha * (x2 - x1)
                y0 = y1 + alpha * (y2 - y1)
                z0 = z1 + alpha * (z2 - z1)

                s = ((x - x0) ** 2 + (y - y0) ** 2 + (z - z0) ** 2) ** 0.5

                if 0 < alpha < 1 and s < eps:
                    delete.append(old_line)

            # delete all old points on the new line
            for d in delete:
                old_map.remove(d)

            # add the new point if it hasn't been in the map 
            point = [x2, y2,  z2]
            already_there = False
            for p in old_map:
                if ((p[0]-point[0])**2 + (p[1]-point[1])**2 + (p[2]-point[2])**2)**0.5 < eps:
                    already_there = True
                    break
            if not already_there:
                old_map.append(point)

        return old_map


        
        """
    def update_map(self, lines):
        old_map = sorted(self.old_map)
        lines = sorted(lines)
        #rospy.logerr('lines: ' + str(lines))
        new_map = []
        map_set = []
        for line in lines:
            line = [line[1][0] - line[0][0], line[1][1] - line[0][1], line[1][2] - line[0][2]]
            #rospy.logerr('line: '+str(line))
            if len(old_map) == 0:
                new_map.append(line)
                if line[0] == 0:
                        tg_l_yx = 9999
                        tg_l_zx = 9999 
                else:
                    tg_l_yx = line[1] / line[0]
                    tg_l_zx = line[2] / line[0]
                map_set.append([tg_l_yx, tg_l_zx])
            
        for coord in old_map:
            if line[0] == 0:
                    tg_l_yx = 9999
                    tg_l_zx = 9999                    
            else:
                tg_l_yx = line[1] / line[0]
                tg_l_zx = line[2] / line[0]
            if coord[0] == 0:
                tg_o_yx = 9999
                tg_o_zx = 9999 
            else:
                tg_o_yx = coord[1] / coord[0]
                tg_o_zx = coord[2] / coord[0]
                
            if tg_l_yx == tg_o_yx and tg_l_zx == tg_o_zx and [tg_l_yx, tg_l_zx] not in map_set:
                #rospy.logerr('line: ' + str(line))
                new_map.append(line)
                map_set.append([tg_l_yx, tg_l_zx])
                if not (line[0] >= coord[0] and line[1] >= coord[1] and line[2] >= coord[2]):
                    new_map.append(coord)
                    
        return sorted(new_map)"""
        
    def convert(self, array):
        marker_arr = MarkerArray()
        for i, el in enumerate(array):
            m = Marker()
            m.header.stamp = rospy.Time.now()
            m.header.frame_id = 'ant1/odom'
            m.id = i
            m.scale.x = 0.1
            m.scale.y = 0.1
            m.scale.z = 0.1
            m.pose.position.x = el[0]
            m.pose.position.y = el[1]
            m.pose.position.z = el[2]
            m.pose.orientation.w = 1
            m.color.a = 1
            m.color.r = 1
            m.color.g = 1
            m.color.b = 1
            m.lifetime.nsecs = 200000000
            m.type = 2
            marker_arr.markers.append(m)
        return marker_arr
        
if __name__ == '__main__':
    rospy.init_node('map_updating')
    al = MapUpdating()
    r = rospy.Rate(10)
    r.sleep()
    
#    pc = PointCloud()
    # pc.points = [{x: 0, y: 1, z: 0}, {x: 0, y: 0.5, z: 0}]
#    p1 = Point32(0, 1, 0)
#    p2 = Point32(0, 0.5, 0)
#    pc.points = [p1, p2]
    
#    al.deserialized_callback(pc)
    
    rospy.spin()
    



