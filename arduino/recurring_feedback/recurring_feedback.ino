/*
 * Programm: Feedback vibro-motors control
 * Author: Moscowsky AD moscowskyad@gmail.com
 */
unsigned long time;
int motorsN = 7;
int motorsPins[7];
int motorsStatus[7];

void setup() {
  Serial.begin(9600);
  /*while(!Serial.find('#'));
  while(motorsN < '1' || motorsN > '7'){
   motorsN = Serial.read();
  }
  motorsN -= '0';*/
  for ( int i = 0; i < motorsN; i++){
    motorsPins[i] = i+2;
    motorsStatus[i] = 0;
  }
  for( int i = 0; i < motorsN; i++){
    pinMode(motorsPins[i], OUTPUT);  
  }
}
/*формат строки #10254*, где 10254 - состояния моторов*/
/*не знаю, будет ли работать при смене режимов*/
void loop() {
  time = millis();
  Serial.print(motorsN);
  Serial.print('\n');
  Serial.print("Pins:");
  Serial.print('\n');
  for(int i = 0; i < motorsN; i++){
    Serial.print(motorsPins[i]);
    Serial.print('\n');
  }
  Serial.print("Status:");
  Serial.print('\n');
  for(int i = 0; i < motorsN; i++){
    Serial.print(motorsStatus[i]);
    Serial.print('\n');
  }
  
  int motor_status = -1;
  int motor = -1;
  if( Serial.available() > 0 ){/*обработка данных с порта*/
    delay(100);
    
    while(!Serial.find('#'));
    motor = Serial.read();
    while(motor < '0'){
      motor = Serial.read();
    }
    motor -= '0';
    
    Serial.read();

    while(motor_status < '0'){
      motor_status = Serial.read();
    }
    motor_status -= '0';

    motorsStatus[motor-1] = motor_status;
  }

  for(int i = 0; i < motorsN; i++){
    if(motorsStatus[i] == 0 ) /*когда подан 0 = выключен*/
      digitalWrite(motorsPins[i], LOW);
    else /*когда пришло время для сигнала*/
      digitalWrite(motorsPins[i], HIGH);
  }
}
